$(function(){
	"use strict";
	ImgChange();
	ImageHidden();
		
	// TOGGLE NAV	
	$( "#global-nav-area" ).hide();
	//var flg = 'default';
	
	$( "#toggle" ).click( function(){
		SPMenuOPEN();
	});
	$( "#global-nav-area #global-nav li a" ).click( function(){
		SPMenuOPEN();
	});
	
	var topBtn = $('#page-top');    
	topBtn.hide();
	
	//スクロールが500に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
		
		//TOP以外のページの時にスクロールが一番上にある時ヘッダーの背景の透明度を無しにする。
		if($('#top').length){
			
		} else {
						
		}
	});
	
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 700);
		return false;
	});
	
	$(window).on('load', function(){
		
	});	
	
	
		
	$('a[href^=#]').click(function(){		
		var speed = 700;
		var href= $(this).attr("href");
		var target = $(href === "#" || href === "" ? 'html' : href);
		var position = 0;
		
		var widimage = window.innerWidth;
		if($(this).hasClass('is-pagescroll')){
			if( widimage < 769 ){	
				position = target.offset().top - $('header').height();
			} else {
				position = target.offset().top;
			}
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;
		} else {
			position = target.offset().top;			
			$("html, body").animate({scrollTop:position}, speed, "swing");
			return false;
		}
	});
	
	$(window).on('load resize', function(){
		ImgChange();
		setTimeout(function(){
			SPMenuHeight();
			ImageHidden();
		},100);
					
	});
	
	if(navigator.userAgent.indexOf('Android') > 0){
        
    }
	
	$('.js-link').on('click', function(e){
		//伝播をストップ
		e.stopPropagation();
		e.preventDefault();
		
		//リンクを取得して飛ばす
		location.href = $(this).attr('data-url');
	});
		
});


function  ImgChange(){
	"use strict";
	// スマホ画像切替
	var widimage = window.innerWidth;
	//var widimage = parseInt($(window).width()) + 16;
	
	if( widimage < 1001 ){
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
		
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 769 ){
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 481 ){
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}

}

function  SPMenuHeight(){
	"use strict";
	var header_Height = $('header').height();
	var menu_Height = window.innerHeight - header_Height;
	//var menu_Height = window.innerHeight - 150;
	$('#global-nav-area').css('top',header_Height + 'px');
	$('#global-nav-area').css('height',menu_Height + 'px');
	$('#global-nav-area').css('overflow-y','scroll');
	$('#global-nav-area').css('-webkit-overflow-scrolling','touch');
}
function  SPMenuOPEN(){
	"use strict";
	var flg = 'default';
	$( "#toggle-btn-icon" ).toggleClass( "close" );

	if( flg === "default" ){
		
		flg = "close";

		$( "#global-nav-area" ).addClass( "show" );
		
	}else{
		
		flg = "default";
		$( "#global-nav-area" ).removeClass( "show" );
	}

	$("#global-nav-area").slideToggle('slow');
}

function  ImageHidden(){
	"use strict";
	var image_Width = $('.l-infoBlock .l-infoBlock-table .l-info a .p-image img').width();
	var image_Height = image_Width * 0.558;
	var image_Width1 = $('.l-searchBlock .l-searchBlock-table .l-search a .p-image img').width();
	var image_Height1 = image_Width1 * 0.558;
	var image_Width2 = $('.l-searchBlock2 .l-searchBlock2-table .l-search2 a .p-image img').width();
	var image_Height2 = image_Width2 * 0.558;
	$('.l-infoBlock .l-infoBlock-table .l-info a .p-image').css('height',image_Height + 'px');
	$('.l-searchBlock .l-searchBlock-table .l-search a .p-image').css('height',image_Height1 + 'px');
	$('.l-searchBlock2 .l-searchBlock2-table .l-search2 a .p-image').css('height',image_Height2 + 'px');
}



