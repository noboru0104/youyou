<!doctype html>
<html lang="ja">
<head>
<?php include("../head.php"); ?>
<title>検索｜おかやま商工会エリア 遊youさんぽ</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/css/search/search.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/js/search/search.js"></script>
<script type="text/javascript" src="/js/common/jquery.matchHeight.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../header.php"); ?>
<div id="wrapper">
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="/images/search/h2_block01_pc.png" alt="検索単語"></h2>
            <div class="l-kensaku">
            	<div class="p-message">さらに絞り込む</div>
                <div class="l-kensaku01">
                	<select name="search_category">
                    	<option value="all">すべて</option>
                        <option value="gourmet">グルメ</option>
                        <option value="shopping">ショッピング</option>
                        <option value="spot">みどころ</option>
                    </select>
                </div>
                <div class="l-kensaku02">
                	<select name="search_area">
                    	<option value="all">すべて</option>
                        <option value="area01">岡山北</option>
                        <option value="area02">岡山西</option>
                        <option value="area03">岡山南</option>
                        <option value="area04">吉備中央町</option>
                        <option value="area05">瀬戸内市</option>
                        <option value="area06">赤磐</option>
                        <option value="area07">備前東</option>
                        <option value="area08">和気</option>
                        <option value="area09">つくぼ</option>
                        <option value="area10">総社吉備路</option>
                        <option value="area11">真備船穂</option>
                        <option value="area12">浅口</option>
                        <option value="area13">備中西</option>
                        <option value="area14">備北</option>
                        <option value="area15">阿哲</option>
                        <option value="area16">真庭</option>
                        <option value="area17">作州津山</option>
                        <option value="area18">鏡野町</option>
                        <option value="area19">久米郡</option>
                        <option value="area20">みまさか</option>
                    </select>
                </div>
                <div class="l-btn">
                	<input type="submit" value="" class="p-search">
                </div>
            </div>
        </div>
    </div>
    
    <?php include("../search02.php"); ?> 
    
</div>
<?php include("../footer.php"); ?>
</body>
</html>
