<?php
/**
 * Twenty Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

/**
 * Twenty Nineteen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

if ( ! function_exists( 'twentynineteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function twentynineteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Nineteen, use a find and replace
		 * to change 'twentynineteen' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'twentynineteen', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'twentynineteen' ),
				'footer' => __( 'Footer Menu', 'twentynineteen' ),
				'social' => __( 'Social Links Menu', 'twentynineteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'twentynineteen' ),
					'shortName' => __( 'S', 'twentynineteen' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'twentynineteen' ),
					'shortName' => __( 'M', 'twentynineteen' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'twentynineteen' ),
					'shortName' => __( 'L', 'twentynineteen' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'twentynineteen' ),
					'shortName' => __( 'XL', 'twentynineteen' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

		// Editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Primary', 'twentynineteen' ),
					'slug'  => 'primary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
				),
				array(
					'name'  => __( 'Secondary', 'twentynineteen' ),
					'slug'  => 'secondary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
				),
				array(
					'name'  => __( 'Dark Gray', 'twentynineteen' ),
					'slug'  => 'dark-gray',
					'color' => '#111',
				),
				array(
					'name'  => __( 'Light Gray', 'twentynineteen' ),
					'slug'  => 'light-gray',
					'color' => '#767676',
				),
				array(
					'name'  => __( 'White', 'twentynineteen' ),
					'slug'  => 'white',
					'color' => '#FFF',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'twentynineteen_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentynineteen_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer', 'twentynineteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'twentynineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'twentynineteen_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function twentynineteen_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'twentynineteen_content_width', 640 );
}
add_action( 'after_setup_theme', 'twentynineteen_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function twentynineteen_scripts() {
	wp_enqueue_style( 'twentynineteen-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );

	wp_style_add_data( 'twentynineteen-style', 'rtl', 'replace' );

	if ( has_nav_menu( 'menu-1' ) ) {
		wp_enqueue_script( 'twentynineteen-priority-menu', get_theme_file_uri( '/js/priority-menu.js' ), array(), '1.1', true );
		wp_enqueue_script( 'twentynineteen-touch-navigation', get_theme_file_uri( '/js/touch-keyboard-navigation.js' ), array(), '1.1', true );
	}

	wp_enqueue_style( 'twentynineteen-print-style', get_template_directory_uri() . '/print.css', array(), wp_get_theme()->get( 'Version' ), 'print' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'twentynineteen_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentynineteen_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'twentynineteen_skip_link_focus_fix' );

/**
 * Enqueue supplemental block editor styles.
 */
function twentynineteen_editor_customizer_styles() {

	wp_enqueue_style( 'twentynineteen-editor-customizer-styles', get_theme_file_uri( '/style-editor-customizer.css' ), false, '1.1', 'all' );

	if ( 'custom' === get_theme_mod( 'primary_color' ) ) {
		// Include color patterns.
		require_once get_parent_theme_file_path( '/inc/color-patterns.php' );
		wp_add_inline_style( 'twentynineteen-editor-customizer-styles', twentynineteen_custom_colors_css() );
	}
}
add_action( 'enqueue_block_editor_assets', 'twentynineteen_editor_customizer_styles' );

/**
 * Display custom color CSS in customizer and on frontend.
 */
function twentynineteen_colors_css_wrap() {

	// Only include custom colors in customizer or frontend.
	if ( ( ! is_customize_preview() && 'default' === get_theme_mod( 'primary_color', 'default' ) ) || is_admin() ) {
		return;
	}

	require_once get_parent_theme_file_path( '/inc/color-patterns.php' );

	$primary_color = 199;
	if ( 'default' !== get_theme_mod( 'primary_color', 'default' ) ) {
		$primary_color = get_theme_mod( 'primary_color_hue', 199 );
	}
	?>

	<style type="text/css" id="custom-theme-colors" <?php echo is_customize_preview() ? 'data-hue="' . absint( $primary_color ) . '"' : ''; ?>>
		<?php echo twentynineteen_custom_colors_css(); ?>
	</style>
	<?php
}
add_action( 'wp_head', 'twentynineteen_colors_css_wrap' );

// タイトルのセパレータ
function change_separator() {
  return "|"; // ここに変更したい区切り文字を書く
}
add_filter('document_title_separator', 'change_separator');

//add_action( 'init', 'create_post_type' );

//カスタム投稿の追加と、カテゴリー、タグ機能の追加
function my_custom_init() {
	//カスタム投稿「detail」の設定
	register_post_type( 'detail', array(
        'label' => '新着情報の投稿',
        'public' => true,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields' ,'comments' ),
        'menu_position' => 5,
        'has_archive' => true
    ));//
//	
//    //カテゴリタイプの設定（カスタムタクソノミーの設定）
//      register_taxonomy(
//        'examplecat', //カテゴリー名（任意）
//        'detail', //カスタム投稿名
//        array(
//          'hierarchical' => true, //カテゴリータイプの指定
//          'update_count_callback' => '_update_post_term_count',
//          //ダッシュボードに表示させる名前
//          'label' => '新着情報のカテゴリー', 
//          'public' => true,
//          'show_ui' => true
//        )
//      );    
//    //タグタイプの設定（カスタムタクソノミーの設定）
//      register_taxonomy(
//        'exampletag', //タグ名（任意）
//        'detail', //カスタム投稿名
//        array(
//          'hierarchical' => false, //タグタイプの指定（階層をもたない）
//          'update_count_callback' => '_update_post_term_count',
//          //ダッシュボードに表示させる名前
//          'label' => '新着情報のタグ', 
//          'public' => true,
//          'show_ui' => true
//        )
//      );
  
}
add_action( 'init', 'my_custom_init' );

function add_custom_post() {
  register_post_type(
    'detail',
    array(
      'label' => '新着情報',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array(
                      'title',
                      'editor',
                      'thumbnail',
                      'revisions',
                      'excerpt',
                      'custom-fields',
                      )
    )
  );
  //カテゴリを投稿と共通設定にする
  register_taxonomy_for_object_type('category', 'detail');
  //タグを投稿と共通設定にする
  register_taxonomy_for_object_type('post_tag', 'detail');
}
add_action('init', 'add_custom_post');







//information
function information() {
	
	?>
<div class="l-infoBlock">
            
	<?php

        $args = array(
            'post_type' => 'detail',
            'posts_per_page' => '4',
			'post_status' => 'publish',
			'order'=>'DESC',
			'orderby'=>'post_date',
        );
        
        $the_query = new WP_Query($args);
    ?>
    <div class="l-inner">
        <h2><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/h2_infoBlock_pc.png" alt="新着情報"></h2>
        <div class="l-infoBlock-table">
            <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query-> have_posts() ) : $the_query->the_post();
				$category = get_the_category();
				$cat_id   = $category[0]->cat_ID;
				$cat_name = $category[0]->cat_name;
				$cat_slug = $category[0]->category_nicename;
				
				$tags = get_the_tags();
				$tag_id   = $tags[0]->term_id;
				$tag_name = $tags[0]->name;
				$tag_slug = $tags[0]->slug;
				
				$title = CFS()->get('title');
				$photo = CFS()->get('photo');
				$message = CFS()->get('message');
				$map = CFS()->get('map');
			
			 ?><div class="l-info">
                <a href="<?php the_permalink(); ?>">
                    <p class="p-image"><img src="<?php echo $photo; ?>" alt="<?php echo $title; ?>"></p>
                    <p class="p-category"><span data-url="<?php echo home_url();?>/detail_search/?category=<?php echo $cat_slug; ?>" class="js-link p-<?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span><span data-url="<?php echo home_url();?>/detail_search/?area=<?php echo $tag_slug; ?>" class="js-link p-<?php echo $tag_slug; ?>"><?php echo $tag_name; ?></span></p>
                    <p class="p-date"><?php echo get_post_time('Y/m/d(D)'); ?></p>
                    <p class="p-title"><?php echo $title; ?></p>
                </a>
            </div><?php endwhile; endif; ?>
            
            
            
            <!--<div class="l-info">
                <a href="">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_infoBlock_02.png" alt=""></p>
                    <p class="p-category"><span class="p-shopping">ショッピング</span><span class="p-area01">岡山西</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div><div class="l-info">
                <a href="">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_infoBlock_03.png" alt=""></p>
                    <p class="p-category"><span class="p-spot">みどころ</span><span class="p-area04">阿哲</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div><div class="l-info">
                <a href="">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_infoBlock_04.png" alt=""></p>
                    <p class="p-category"><span class="p-shopping">ショッピング</span><span class="p-area03">浅口</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div>-->
        </div>
    </div>
</div>

	<?php
}

//information（エリア限定した状態で表示）
function information2() {
	
	?>
<div class="l-infoBlock">
            
	<?php
		$now_id = get_the_ID();
		$tags0 = get_the_tags($now_id);
		$tag_slug0 = $tags0[0]->slug;
        $args = array(
            'post_type' => 'detail',
            'posts_per_page' => '4',
			'post__not_in'=> array($now_id),
			'post_status' => 'publish',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'post_tag',
					'field' => 'slug',
					'terms' => $tag_slug0,
				),
			),
			'order'=>'DESC',
			'orderby'=>'post_date',
        );
        
        $the_query = new WP_Query($args);
    ?>
    <div class="l-inner">
        <h2><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/h2_infoBlock_pc.png" alt="新着情報"></h2>
        <div class="l-infoBlock-table">
            <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query-> have_posts() ) : $the_query->the_post();
				$category = get_the_category();
				$cat_id   = $category[0]->cat_ID;
				$cat_name = $category[0]->cat_name;
				$cat_slug = $category[0]->category_nicename;
				
				$tags = get_the_tags();
				$tag_id   = $tags[0]->term_id;
				$tag_name = $tags[0]->name;
				$tag_slug = $tags[0]->slug;
				
				$title = CFS()->get('title');
				$photo = CFS()->get('photo');
				$message = CFS()->get('message');
				$map = CFS()->get('map');
			
			 ?><div class="l-info">
                <a href="<?php the_permalink(); ?>">
                    <p class="p-image"><img src="<?php echo $photo; ?>" alt="<?php echo $title; ?>"></p>
                    <p class="p-category"><span data-url="<?php echo home_url();?>/detail_search/?category=<?php echo $cat_slug; ?>" class="js-link p-<?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span><span data-url="<?php echo home_url();?>/detail_search/?area=<?php echo $tag_slug; ?>" class="js-link p-<?php echo $tag_slug; ?>"><?php echo $tag_name; ?></span></p>
                    <p class="p-date"><?php echo get_post_time('Y/m/d(D)'); ?></p>
                    <p class="p-title"><?php echo $title; ?></p>
                </a>
            </div><?php endwhile; endif; ?>
            
            
            
            <!--<div class="l-info">
                <a href="">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_infoBlock_02.png" alt=""></p>
                    <p class="p-category"><span class="p-shopping">ショッピング</span><span class="p-area01">岡山西</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div><div class="l-info">
                <a href="">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_infoBlock_03.png" alt=""></p>
                    <p class="p-category"><span class="p-spot">みどころ</span><span class="p-area04">阿哲</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div><div class="l-info">
                <a href="">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_infoBlock_04.png" alt=""></p>
                    <p class="p-category"><span class="p-shopping">ショッピング</span><span class="p-area03">浅口</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div>-->
        </div>
    </div>
</div>

	<?php
}

//search01
function search01() {
	
	?>
<div class="l-searchBlock">
<?php
        
                //$args = array(
//                    'post_type' => 'interview',
//                    'interview_category' => 'recommend',
//                    'posts_per_page' => '5',
//                    'order'=>'DESC',
//                    'orderby'=>'meta_value_num',
//                    'meta_key'=>'post_views_count'
//                );
//                
//                $the_query = new WP_Query($args);
            ?>
    <div class="l-inner">
        <h2><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/h2_searchBlock_pc.png" alt="エリア別に探す"></h2>
        <div class="l-searchBlock-table">
            <div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area01">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_01.png" alt=""></p>
                    <p class="p-category"><span class="p-area01">岡山北</span></p>
                    <p class="p-title">のどかな田園と里山に抱かれた、<br>岡山市近郊の食と歴史のエリア</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area02">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_02.png" alt=""></p>
                    <p class="p-category"><span class="p-area02">岡山西</span></p>
                    <p class="p-title">岡山・倉敷から少し足を延ばして…<br>桃太郎ゆかりの史跡や自然を満喫♪</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area03">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_03.png" alt=""></p>
                    <p class="p-category"><span class="p-area03">岡山南</span></p>
                    <p class="p-title">花見の名所がたくさんあるエリア農業体験やイベントもおすすめ</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area04">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_04.png" alt=""></p>
                    <p class="p-category"><span class="p-area04">吉備中央町</span></p>
                    <p class="p-title">自然豊かな美しい景観と魅力的なスポットをめぐる</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area05">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_05.png" alt=""></p>
                    <p class="p-category"><span class="p-area05">瀬戸内市</span></p>
                    <p class="p-title">風光明媚でロケ地も多いエリア<br>季節を感じてぶらり散策</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area06">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_06.png" alt=""></p>
                    <p class="p-category"><span class="p-area06">赤磐</span></p>
                    <p class="p-title">自慢の果物や特産品を堪能し、<br>ワインや日本酒をお土産に</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area07">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_07.png" alt=""></p>
                    <p class="p-category"><span class="p-area07">備前東</span></p>
                    <p class="p-title">新鮮魚介とカキオコを味わい、<br>備前焼の凛とした美しさに触れる</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area08">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_08.png" alt=""></p>
                    <p class="p-category"><span class="p-area08">和気</span></p>
                    <p class="p-title">清流に育まれた自然と美しい藤の花。<br>季節のイベントも魅力的な町</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area09">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_09.png" alt=""></p>
                    <p class="p-category"><span class="p-area09">つくぼ</span></p>
                    <p class="p-title">古い街並みと新しいスポットが共存。<br>岡山からも倉敷からもアクセス良好</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area10">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_10.png" alt=""></p>
                    <p class="p-category"><span class="p-area10">総社吉備路</span></p>
                    <p class="p-title">古代の史跡が点在する吉備路をドライブやサイクリングでぶらり</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area11">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_11.png" alt=""></p>
                    <p class="p-category"><span class="p-area11">真備船穂</span></p>
                    <p class="p-title">史跡と自然の恵みいっぱい！<br>楽しみながらゆっくり散策はいかが？</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area12">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_12.png" alt=""></p>
                    <p class="p-category"><span class="p-area12">浅口</span></p>
                    <p class="p-title">風光明媚な「天文のまち」は<br>お楽しみスポットいっぱい！</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area13">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_13.png" alt=""></p>
                    <p class="p-category"><span class="p-area13">備中西</span></p>
                    <p class="p-title">のどかな里山の風景の中で、<br>備中の歴史や自然にふれる</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area14">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_14.png" alt=""></p>
                    <p class="p-category"><span class="p-area14">備北</span></p>
                    <p class="p-title">有名な観光スポットの点在する<br>山あいのドライブルート</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area15">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_15.png" alt=""></p>
                    <p class="p-category"><span class="p-area15">阿哲</span></p>
                    <p class="p-title">千屋牛と鍾乳洞だけじゃない。<br>グルメと特産品を見つけに行こう！</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area16">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_16.png" alt=""></p>
                    <p class="p-category"><span class="p-area16">真庭</span></p>
                    <p class="p-title">よく知られた観光スポット目白押し。<br>休日ドライブに最適なエリア！</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area17">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_17.png" alt=""></p>
                    <p class="p-category"><span class="p-area17">作州津山</span></p>
                    <p class="p-title">四季を通じて楽しめる観光スポットがあちこちに！</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area18">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_18.png" alt=""></p>
                    <p class="p-category"><span class="p-area18">鏡野町</span></p>
                    <p class="p-title">自然が作った絶景と自然がくれた恵みを、自然と一緒に満喫しよう！</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area19">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_19.png" alt=""></p>
                    <p class="p-category"><span class="p-area19">久米郡</span></p>
                    <p class="p-title">家族でお出かけも安心！<br>ゆっくりできるスポットがいっぱい！</p>
                </a>
            </div><div class="l-search">
                <a href="<?php echo home_url();?>/detail_search/?area=area20">
                    <p class="p-image"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/img_searchBlock_20.png" alt=""></p>
                    <p class="p-category"><span class="p-area20">みまさか</span></p>
                    <p class="p-title">歴史と温泉の郷のスポットで地元の美味と楽しさを満喫しよう！</p>
                </a>
            </div>
        </div>
    </div>
</div>

	<?php
}

/**
 * SVG Icons class.
 */
require get_template_directory() . '/classes/class-twentynineteen-svg-icons.php';

/**
 * Custom Comment Walker template.
 */
require get_template_directory() . '/classes/class-twentynineteen-walker-comment.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
