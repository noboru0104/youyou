<?php
/*
Template Name: detail_search
*/
?>
<?php
	session_start();
?>
<?php get_header(); ?>

<?php 
	$prev_site = $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
	$now_site = '/detail_search/';
	//クエリがある場合-ヘッダークリック
	if(isset($_GET['category'])){
		//var_dump('ヘッダークリック');
		$_SESSION['search_category'] = $_GET['category'];
		if($_SESSION['search_category'] === 'all'){
			$get_cat = get_categories();
			foreach($get_cat as $val) {
				$category_list_slug[$val->cat_ID] = $val->slug;
			}
			$_SESSION['search_category'] = $category_list_slug;
		} else {
			$_SESSION['search_category'] = $_GET['category'];
		}		
		$get_tag = get_tags();
		foreach($get_tag as $val2) {
			$tag_list_slug[$val2->term_id] = $val2->slug;
		}
		
		$_SESSION['search_area'] = $tag_list_slug;
		$now_page = 0;
	}
	//ページ内の「エリア別に探す」をクリックした場合
	else if(isset($_GET['area'])){
		//var_dump('ヘッダークリック');
		$get_cat = get_categories();
		foreach($get_cat as $val) {
			$category_list_slug[$val->cat_ID] = $val->slug;
		}
		$_SESSION['search_category'] = $category_list_slug;
		
		$_SESSION['search_area'] = $_GET['area'];
		$now_page = 0;
	} else {
		//同じページでの遷移=submitでの読み込み
		if (strpos($prev_site, $now_site) !== false) {
			//ページング中にドロップダウンが変更されて検索された時
			if(isset($_POST['search_category']) || isset($_POST['search_area'])){
				//var_dump('ドロップダウンが変更された時');
				$now_page = 1;
				//すべてを選択して検索ボタンを押した場合はすべてのカテゴリーを取得して検索に持っていく
				if($_POST['search_category'] === 'all'){
					$get_cat = get_categories();
					foreach($get_cat as $val) {
						$category_list_slug[$val->cat_ID] = $val->slug;
					}
					$_SESSION['search_category'] = $category_list_slug;
				} else {
					$_SESSION['search_category'] = $_POST['search_category'];
				}
				//すべてを選択して検索ボタンを押した場合はすべてのタグを取得して検索に持っていく
				if($_POST['search_area'] === 'all'){
					$get_tag = get_tags();
					foreach($get_tag as $val2) {
						$tag_list_slug[$val2->term_id] = $val2->slug;
					}
					$_SESSION['search_area'] = $tag_list_slug;
				} else {
					$_SESSION['search_area'] = $_POST['search_area'];		
				}							
			} elseif(isset($_SESSION['search_category']) || $_SESSION['search_category'] === '' || isset($_SESSION['search_area']) || $_SESSION['search_area'] === '') {
				//var_dump('パターン2');
				$now_page = 0;
			} else {
				//ページング中
				$_SESSION['search_category'] = $_SESSION['search_category'];
				$_SESSION['search_area'] = $_SESSION['search_area'];
				$now_page = 0;
			}
		} else {
					
			//別ページからの遷移=初期表示
			//var_dump('初期表示');
			$get_cat = get_categories();
			foreach($get_cat as $val) {
				$category_list_slug[$val->cat_ID] = $val->slug;
			}
			$_SESSION['search_category'] = $category_list_slug;
			
			$get_tag = get_tags();
			foreach($get_tag as $val2) {
				$tag_list_slug[$val2->term_id] = $val2->slug;
			}
			
			$_SESSION['search_area'] = $tag_list_slug;
			$now_page = 0;			
		}		
	}
//var_dump($_SESSION['search_category']);
//var_dump($_SESSION['search_area']);
?>

<div id="wrapper">
    <form action="<?php echo home_url();?>/detail_search/" method="post">
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/search/h2_block01_pc.png" alt="検索単語"></h2>
            
            <div class="l-kensaku">
            	<div class="p-message">さらに絞り込む</div>
                
                <div class="l-kensaku01">
                    <?php
                        if($_SESSION['search_category'] != ''){
                            $search_category = $_SESSION['search_category'];
                        } else {
                            $search_category = '';
                        }
                    ?>
                    <?php
                        if($_SESSION['search_area'] != ''){
                            $search_area = $_SESSION['search_area'];
                        } else {
                            $search_area = '';
                        }
                    ?>
                    <select method="post" name="search_category">
                        <option value="all">すべて</option>
                        <option value="gourmet"
                        <?php
                            if($_SESSION['search_category'] != ''){
                                if($_SESSION['search_category'] === 'gourmet'){
                                    echo 'selected';
                                }
                            }
                        ?>>グルメ</option>
                        <option value="shopping"
                        <?php
                            if($_SESSION['search_category'] != ''){
                                if($_SESSION['search_category'] === 'shopping'){
                                    echo 'selected';
                                }
                            }
                        ?>>ショッピング</option>
                        <option value="spot"
                        <?php
                            if($_SESSION['search_category'] != ''){
                                if($_SESSION['search_category'] === 'spot'){
                                    echo 'selected';
                                }
                            }
                        ?>>みどころ</option>
                    </select>
                </div>
                <div class="l-kensaku02">
                    <select method="post" name="search_area">
                        <option value="all">すべて</option>
                        <option value="area01"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area01'){
                                    echo 'selected';
                                }
                            }
                        ?>>岡山北</option>
                        <option value="area02"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area02'){
                                    echo 'selected';
                                }
                            }
                        ?>>岡山西</option>
                        <option value="area03"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area03'){
                                    echo 'selected';
                                }
                            }
                        ?>>岡山南</option>
                        <option value="area04"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area04'){
                                    echo 'selected';
                                }
                            }
                        ?>>吉備中央町</option>
                        <option value="area05"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area05'){
                                    echo 'selected';
                                }
                            }
                        ?>>瀬戸内市</option>
                        <option value="area06"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area06'){
                                    echo 'selected';
                                }
                            }
                        ?>>赤磐</option>
                        <option value="area07"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area07'){
                                    echo 'selected';
                                }
                            }
                        ?>>備前東</option>
                        <option value="area08"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area08'){
                                    echo 'selected';
                                }
                            }
                        ?>>和気</option>
                        <option value="area09"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area09'){
                                    echo 'selected';
                                }
                            }
                        ?>>つくぼ</option>
                        <option value="area10"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area10'){
                                    echo 'selected';
                                }
                            }
                        ?>>総社吉備路</option>
                        <option value="area11"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area11'){
                                    echo 'selected';
                                }
                            }
                        ?>>真備船穂</option>
                        <option value="area12"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area12'){
                                    echo 'selected';
                                }
                            }
                        ?>>浅口</option>
                        <option value="area13"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area13'){
                                    echo 'selected';
                                }
                            }
                        ?>>備中西</option>
                        <option value="area14"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area14'){
                                    echo 'selected';
                                }
                            }
                        ?>>備北</option>
                        <option value="area15"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area15'){
                                    echo 'selected';
                                }
                            }
                        ?>>阿哲</option>
                        <option value="area16"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area16'){
                                    echo 'selected';
                                }
                            }
                        ?>>真庭</option>
                        <option value="area17"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area17'){
                                    echo 'selected';
                                }
                            }
                        ?>>作州津山</option>
                        <option value="area18"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area18'){
                                    echo 'selected';
                                }
                            }
                        ?>>鏡野町</option>
                        <option value="area19"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area19'){
                                    echo 'selected';
                                }
                            }
                        ?>>久米郡</option>
                        <option value="area20"
                        <?php
                            if($_SESSION['search_area'] != ''){
                                if($_SESSION['search_area'] === 'area20'){
                                    echo 'selected';
                                }
                            }
                        ?>>みまさか</option>
                    </select>
                </div>
                <div class="l-btn">
                    <input type="submit" value="" class="p-search">
                </div>
            </div>
        </div>
    </div>
    
    <div class="l-searchBlock2">
        <div class="l-inner">
            <div class="l-searchBlock2-table">
                 <?php
                    
					if($now_page === 0) {
						$paged = get_query_var( 'paged', 1 );	
					} else {
						$paged = 1;	
					}
					
					$args = array(
						'post_type' => 'detail',
						'posts_per_page' => '8',
						//'post__not_in'=> array($not_id),
						'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'category',
								'field' => 'slug',
								'terms' => $_SESSION['search_category'],
							),
							'relation' => 'AND',
							array(
								'taxonomy' => 'post_tag',
								'field' => 'slug',
								'terms' => $_SESSION['search_area'],
							),
						),
						'order'=>'DESC',
                        'paged' => $paged,							
					);
					
					$the_query = new WP_Query($args);
				?>
                <?php if ($the_query->have_posts()) : ?>
                <?php while ($the_query-> have_posts() ) : $the_query->the_post();
				
					$category = get_the_category();
					$cat_id   = $category[0]->cat_ID;
					$cat_name = $category[0]->cat_name;
					$cat_slug = $category[0]->category_nicename;
					
					$tags = get_the_tags();
					$tag_id   = $tags[0]->term_id;
					$tag_name = $tags[0]->name;
					$tag_slug = $tags[0]->slug;
					
					$title = CFS()->get('title');
					$photo = CFS()->get('photo');
					$message = CFS()->get('message');
					$map = CFS()->get('map');
				
				?><div class="l-search2">
                    <a href="<?php the_permalink(); ?>" class="clear">
                        <p class="p-image">
                            <img src="<?php echo $photo; ?>" alt="<?php echo $title; ?>">
                        </p><p class="p-category">
                            <span class="p-<?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span><span class="p-<?php echo $tag_slug; ?>"><?php echo $tag_name; ?></span>
                        </p><p class="p-date">
                            <?php echo get_post_time('Y/m/d(D)'); ?>
                        </p><p class="p-title">
                            <?php echo $title; ?>
                        </p>
                    </a>
                </div><?php endwhile; wp_reset_postdata(); ?>
                <!-- ナビゲーション -->                            
                <div class="activity-nav">
                <?php
                    global $wp_rewrite;
                    //var_dump(get_pagenum_link(1));
                    //var_dump(get_query_var('page'));
                    $paginate_base = get_pagenum_link(1);
                    if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
                        $paginate_format = '';
                        $paginate_base = add_query_arg('paged', '%#%');
                    } else {
                        $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/') .
                        user_trailingslashit('page/%#%/', 'paged');
                        $paginate_base .= '%_%';
                    }
                    echo paginate_links( array(
                    'base' => $paginate_base,
                    'format' => $paginate_format,
                    'total' => $the_query->max_num_pages,
                    'end_size'    => 0,
                    'mid_size'    => 1,
                    'prev_next'   => false,
                    'current' => ($paged ? $paged : 1),
                )); ?>
                </div>
                <?php wp_reset_postdata(); ?>
                <?php endif;  ?>
        	</div>
        </div>
    </div>
    </form>
    
</div>

<?php 
	//endwhile;
	//endif;
	
?>

<?php get_footer(); ?>