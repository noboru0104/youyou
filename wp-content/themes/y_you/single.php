<?php get_header(); ?>

<?php 
	//if (have_posts()) :
	//while (have_posts()) :
	
	//$category = get_the_category();
//	$cat_name = $category[0]->cat_name;
//	$cat_slug = get_the_term_list();


	/*if ($terms = get_the_terms($post->ID, 'examplecat')) {
		foreach ( $terms as $term ) {
			echo esc_html($term->name);
		}
	}*/
	
	//endwhile;
	//endif;
	
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
	
	$tags = get_the_tags();
	$tag_id   = $tags[0]->term_id;
	$tag_name = $tags[0]->name;
	$tag_slug = $tags[0]->slug;
  	
	$title = CFS()->get('title');
	$photo = CFS()->get('photo');
	$message = CFS()->get('message');
	$name = CFS()->get('name');
	if(!empty($name)){
		$name = $name;
	} else {
		$name = $title;
	}
	$map = CFS()->get('map');
	$zoom = CFS()->get('zoom');
	if(!empty($zoom)){
		$zoom = $zoom;
	} else {
		$zoom = 17;
	}
	
	$adress = CFS()->get('adress');
	$tel = CFS()->get('tel');
	$time = CFS()->get('time');
	$holiday = CFS()->get('holiday');
	$seat = CFS()->get('seat');
	$fee = CFS()->get('fee');
	$parking = CFS()->get('parking');
	
?>

<div id="wrapper">
    <div class="l-block01">
    	<div class="l-inner700">
        	<div class="l-title_inner">
            	<h2 class="p-title p-<?php echo $cat_slug; ?>"><?php echo $title; ?></h2>
            </div>
            <div class="l-block01-table">
            	<p class="p-date"><?php echo get_post_time('Y/m/d(D)'); ?></p>
                <p class="p-category"><span class="p-<?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span><span class="p-<?php echo $tag_slug; ?>"><?php echo $tag_name; ?></span></p>
            </div>
            <p class="p-image"><img src="<?php echo $photo; ?>"></p>
            <p class="p-message">
            	<?php echo $message; ?>
            </p>            
        </div>
    </div>
    <div class="l-block02">
    	<div class="l-inner1000">
            <div class="l-block02-01">
            	<div class="l-left">
                	<?php
                    	if(!empty($adress)){
					?>
                    	<table>
                    		<?php
								if(!empty($adress)){
							?>
							<tr>
                                <th>住　　所</th>
                                <td><?php echo $adress; ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <?php
								if(!empty($tel)){
							?>
							<tr>
                                <th>電話番号</th>
                                <td><?php echo $tel; ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <?php
								if(!empty($time)){
							?>
							<tr>
                                <th>営業時間</th>
                                <td><?php echo $time; ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <?php
								if(!empty($holiday)){
							?>
							<tr>
                                <th>休　　日</th>
                                <td><?php echo $holiday; ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <?php
								if(!empty($seat)){
							?>
							<tr>
                                <th>席</th>
                                <td><?php echo $seat; ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <?php
								if(!empty($fee)){
							?>
							<tr>
                                <th>料　　金</th>
                                <td><?php echo $fee; ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <?php
								if(!empty($parking)){
							?>
							<tr>
                                <th>駐 車 場</th>
                                <td><?php echo $parking; ?></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </table>
                    <?php
						}
					?>
                </div><?php if(!empty($map)){ ?><div class="l-right">
                	<iframe src="https://maps.google.co.jp/maps?output=embed&z=<?php echo $zoom; ?>&q=<?php echo $name; ?> <?php echo $map; ?>"></iframe><?php } ?>
                </div>
            </div>       
        </div>
    </div>
    
    <?php information2(); ?>
    
</div>

<?php 
	//endwhile;
	//endif;
	
?>

<?php get_footer(); ?>