<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="">
<?php wp_head(); ?>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/reset.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/common.css">
<!-- ▲共通CSS▲ -->
<!-- ▼favicon▼ -->
<!--<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">-->
<!-- ▲favicon▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/script.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/jquery.matchHeight.js"></script>
<!-- ▲共通JS▲ -->
<title>おかやま商工会エリア 遊youさんぽ</title>
<!-- ▼個別CSS▼ -->
<?php if(is_front_page()): ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/top.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/common/animate.css">
<?php elseif(is_page('information')) : ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/information/information.css">
<?php elseif(is_page('detail_search')) : ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/search/search.css">
<?php elseif(is_singular('detail')) : ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/detail/detail.css">
<?php endif; ?>
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<?php if(is_front_page()): ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/top.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/common/jquery.bxslider.min.js"></script>
<?php elseif(is_page('information')) : ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/information/information.js"></script>
<?php elseif(is_page('detail_search')) : ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/search/search.js"></script>
<?php elseif(is_singular('detail')) : ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/detail/detail.js"></script>
<?php endif; ?>
<!-- ▲個別JS▲ -->
</head>
<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left">
                <a href="/"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_header_logo_pc.png" alt="おかやま商工会エリア 遊youさんぽ"></a>
            </div>
            <div class="l-header-center">
            	<h1>岡山の「見たい」「食べたい」「買いたい」をご紹介！ 遊youさんぽ</h1>
                <div>
                    <div><a class="p-gourmet" href="<?php echo home_url();?>/detail_search/?category=gourmet">グルメ<span>GOURMET</span></a></div>
                    <div><a class="p-shopping" href="<?php echo home_url();?>/detail_search/?category=shopping">ショッピング<span>SHOPPING</span></a></div>
                    <div><a class="p-spot" href="<?php echo home_url();?>/detail_search/?category=spot">みどころ<span>SPOT</span></a></div>
                    <div><a class="p-search" href="<?php echo home_url();?>/detail_search/?category=all">エリア別に探す<span>SEARCH</span></a></div>
                    <div><a class="p-information" href="<?php echo home_url();?>/information/">運営者情報<span>INFORMATION</span></a></div>
                </div>
            </div>
            <div class="l-header-right">
            	<!--SPハンバーガーメニュー-->
                <div id="toggle_block" class="sponly0">
                	
                    <div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span>
                        </div>
                    </div><!--/#toggle-->
                </div><!--/#toggle_block-->
            </div>
        </div>
              
        <nav>
            <div id="global-nav-area">
                <ul id="global-nav">
                    <li>
                    	<p><a class="p-gourmet" href="<?php echo home_url();?>/detail_search/?category=gourmet"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_header_01_sp.png" alt="グルメ GOURMET"></a></p>
                    </li><li>
                    	<p><a class="p-shopping" href="<?php echo home_url();?>/detail_search/?category=shopping"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_header_02_sp.png" alt="ショッピング SHOPPING"></a></p>
                    </li><li>
                    	<p><a class="p-spot" href="<?php echo home_url();?>/detail_search/?category=spot"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_header_03_sp.png" alt="みどころ SPOT"></a></p>
                    </li><li>
                    	<p><a href="<?php echo home_url();?>/detail_search/?category=all"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_header_04_sp.png" alt="エリア別に探す SEARCH"></a></p>
                    </li><li>
                    	<p><a href="<?php echo home_url();?>/information/"><img src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_header_05_sp.png" alt="運営者情報 INFORMATION"></a></p>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>