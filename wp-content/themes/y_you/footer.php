<footer>
    <div class="l-footerBlock01">
    	<div class="l-inner1100">
        	<div class="l-footerBlock01-table">
            	<div class="l-left">
                	<p><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_logo_pc.png" alt="おかやま商工会エリア 遊youさんぽ"></p>
                </div>
                <div class="l-right">
                	<p class="l-link"><a href="<?php echo home_url();?>">トップ</a><a href="<?php echo home_url();?>/detail_search/?category=gourmet">グルメ</a><a href="<?php echo home_url();?>/detail_search/?category=shopping">ショッピング</a><a href="<?php echo home_url();?>/detail_search/?category=spot">みどころ</a><a href="<?php echo home_url();?>/detail_search/?category=all">エリア別に探す</a><a href="<?php echo home_url();?>/information/">運営者情報</a></p>
                    <p class="l-adress">
                        岡山商工会女性部連合会<br>
                        〒700-0817　岡山市北区弓之町4-19-401
                    </p>
                    <div class="l-banner">
                    	<p>
                        	<a href="http://www.okasci.or.jp/" target="_blank"><img class="is-imgChange p-banner01" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_01_pc.png" alt="岡山商工会連合会"></a>
                        </p><p>
                        	<a href="http://www.kodutumibin-okayama.com/" target="_blank"><img class="is-imgChange p-banner02" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_02_pc.png" alt="商工会女性部発 岡山おひさま便"></a>
                        </p><p>
                        	<a href="http://www.okasci.or.jp/~community_business/" target="_blank"><img class="is-imgChange p-banner03" src="<?php echo get_stylesheet_directory_uri();?>/images/common/btn_footer_03_pc.png" alt="岡山県定住促進 U・I・Jターンで地域を元気に！"></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-copy">
    	<p>&copy;youyousanpo All Rights Reserved. </p>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>