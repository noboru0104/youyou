
<?php get_header(); ?>

<div class="l-mvBlock">
    <div class="l-mv">
        <ul class="bxslider">
            <li><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv01_pc.png" alt=""></li>
            <li><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv02_pc.png" alt=""></li>
            <li><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/img_mv03_pc.png" alt=""></li>
        </ul>
           
    </div>
</div>
    
<div id="wrapper">
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/h2_block01_pc.png" alt="メニュー"></h2>
            <div class="l-block01-table">
                <p>
                    <a href="<?php echo home_url();?>/detail_search/?category=gourmet"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_block01_01_pc.png" alt="グルメ"></a>
                </p>
                <p>
                    <a href="<?php echo home_url();?>/detail_search/?category=shopping"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_block01_02_pc.png" alt="ショッピング"></a>
                </p>
                <p>
                    <a href="<?php echo home_url();?>/detail_search/?category=spot"><img class="is-imgChange" src="<?php echo get_stylesheet_directory_uri();?>/images/top/btn_block01_03_pc.png" alt="みどころ"></a>
                </p>
            </div>
        </div>
    </div>
    
    <?php information(); ?>
    <?php search01(); ?>  
    
</div>

<?php get_footer(); ?>
