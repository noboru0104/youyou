<div class="l-infoBlock">
    <div class="l-inner">
        <h2><img class="is-imgChange" src="/images/common/h2_infoBlock_pc.png" alt="新着情報"></h2>
        <div class="l-infoBlock-table">
            <div class="l-info">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_infoBlock_01.png" alt=""></p>
                    <p class="p-category"><span class="p-gourmet">グルメ</span><span class="p-area01">岡山北</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div><div class="l-info">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_infoBlock_02.png" alt=""></p>
                    <p class="p-category"><span class="p-shopping">ショッピング</span><span class="p-area01">岡山西</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div><div class="l-info">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_infoBlock_03.png" alt=""></p>
                    <p class="p-category"><span class="p-spot">みどころ</span><span class="p-area04">阿哲</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div><div class="l-info">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_infoBlock_04.png" alt=""></p>
                    <p class="p-category"><span class="p-shopping">ショッピング</span><span class="p-area03">浅口</span></p>
                    <p class="p-date">2019/02/25(Mon)</p>
                    <p class="p-title">タイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトルタイトル</p>
                </a>
            </div>
        </div>
    </div>
</div>