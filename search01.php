<div class="l-searchBlock">
    <div class="l-inner">
        <h2><img class="is-imgChange" src="/images/common/h2_searchBlock_pc.png" alt="エリア別に探す"></h2>
        <div class="l-searchBlock-table">
            <div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_01.png" alt=""></p>
                    <p class="p-category"><span class="p-area01">岡山北</span></p>
                    <p class="p-title">のどかな田園と里山に抱かれた、<br>岡山市近郊の食と歴史のエリア</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_02.png" alt=""></p>
                    <p class="p-category"><span class="p-area01">岡山西</span></p>
                    <p class="p-title">岡山・倉敷から少し足を延ばして…<br>桃太郎ゆかりの史跡や自然を満喫♪</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_03.png" alt=""></p>
                    <p class="p-category"><span class="p-area01">岡山南</span></p>
                    <p class="p-title">花見の名所がたくさんあるエリア農業体験やイベントもおすすめ</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_04.png" alt=""></p>
                    <p class="p-category"><span class="p-area01">吉備中央町</span></p>
                    <p class="p-title">自然豊かな美しい景観と魅力的なスポットをめぐる</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_05.png" alt=""></p>
                    <p class="p-category"><span class="p-area02">瀬戸内市</span></p>
                    <p class="p-title">風光明媚でロケ地も多いエリア<br>季節を感じてぶらり散策</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_06.png" alt=""></p>
                    <p class="p-category"><span class="p-area02">赤磐</span></p>
                    <p class="p-title">自慢の果物や特産品を堪能し、<br>ワインや日本酒をお土産に</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_07.png" alt=""></p>
                    <p class="p-category"><span class="p-area02">備前東</span></p>
                    <p class="p-title">新鮮魚介とカキオコを味わい、<br>備前焼の凛とした美しさに触れる</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_08.png" alt=""></p>
                    <p class="p-category"><span class="p-area02">和気</span></p>
                    <p class="p-title">清流に育まれた自然と美しい藤の花。<br>季節のイベントも魅力的な町</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_09.png" alt=""></p>
                    <p class="p-category"><span class="p-area03">つくぼ</span></p>
                    <p class="p-title">古い街並みと新しいスポットが共存。<br>岡山からも倉敷からもアクセス良好</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_10.png" alt=""></p>
                    <p class="p-category"><span class="p-area03">総社吉備路</span></p>
                    <p class="p-title">古代の史跡が点在する吉備路をドライブやサイクリングでぶらり</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_11.png" alt=""></p>
                    <p class="p-category"><span class="p-area03">真備船穂</span></p>
                    <p class="p-title">史跡と自然の恵みいっぱい！<br>楽しみながらゆっくり散策はいかが？</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_12.png" alt=""></p>
                    <p class="p-category"><span class="p-area03">浅口</span></p>
                    <p class="p-title">風光明媚な「天文のまち」は<br>お楽しみスポットいっぱい！</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_13.png" alt=""></p>
                    <p class="p-category"><span class="p-area04">備中西</span></p>
                    <p class="p-title">のどかな里山の風景の中で、<br>備中の歴史や自然にふれる</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_14.png" alt=""></p>
                    <p class="p-category"><span class="p-area04">備北</span></p>
                    <p class="p-title">有名な観光スポットの点在する<br>山あいのドライブルート</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_15.png" alt=""></p>
                    <p class="p-category"><span class="p-area04">阿哲</span></p>
                    <p class="p-title">千屋牛と鍾乳洞だけじゃない。<br>グルメと特産品を見つけに行こう！</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_16.png" alt=""></p>
                    <p class="p-category"><span class="p-area04">真庭</span></p>
                    <p class="p-title">よく知られた観光スポット目白押し。<br>休日ドライブに最適なエリア！</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_17.png" alt=""></p>
                    <p class="p-category"><span class="p-area05">作州津山</span></p>
                    <p class="p-title">四季を通じて楽しめる観光スポットがあちこちに！</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_18.png" alt=""></p>
                    <p class="p-category"><span class="p-area05">鏡野町</span></p>
                    <p class="p-title">自然が作った絶景と自然がくれた恵みを、自然と一緒に満喫しよう！</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_19.png" alt=""></p>
                    <p class="p-category"><span class="p-area05">久米郡</span></p>
                    <p class="p-title">家族でお出かけも安心！<br>ゆっくりできるスポットがいっぱい！</p>
                </a>
            </div><div class="l-search">
                <a href="">
                    <p class="p-image"><img src="/images/common/img_searchBlock_20.png" alt=""></p>
                    <p class="p-category"><span class="p-area05">みまさか</span></p>
                    <p class="p-title">歴史と温泉の郷のスポットで地元の美味と楽しさを満喫しよう！</p>
                </a>
            </div>
        </div>
    </div>
</div>