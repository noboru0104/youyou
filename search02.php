<div class="l-searchBlock2">
    <div class="l-inner">
        <div class="l-searchBlock2-table">
            <div class="l-search2">
                <a href="" class="clear">
                    <p class="p-image">
                        <img src="/images/common/img_searchBlock_01.png" alt="">
                    </p><p class="p-category">
                        <span class="p-gourmet">グルメ</span><span class="p-area01">岡山北</span>
                    </p><p class="p-date">
                        2019/02/25(Mon)
                    </p><p class="p-title">
                        そば処 ひじり庵
                    </p>
                </a>
            </div><div class="l-search2">
                <a href="" class="clear">
                    <p class="p-image">
                        <img src="/images/common/img_searchBlock_02.png" alt="">
                    </p><p class="p-category">
                        <span class="p-gourmet">グルメ</span><span class="p-area01">岡山北</span>
                    </p><p class="p-date">
                        2019/01/12(Sat)
                    </p><p class="p-title">
                        食工房ぶどうの木舎
                    </p>
                </a>
            </div><div class="l-search2">
                <a href="" class="clear">
                    <p class="p-image">
                        <img src="/images/common/img_searchBlock_03.png" alt="">
                    </p><p class="p-category">
                        <span class="p-gourmet">グルメ</span><span class="p-area01">岡山北</span>
                    </p><p class="p-date">
                        2018/10/21(Sun)
                    </p><p class="p-title">
                        上道ベーカリー ぱんとまいむ
                    </p>
                </a>
            </div>
        </div>
        <!-- ナビゲーション -->
        <div class="activity-nav">
            <a class='page-numbers' href=''>&lt;</a>&nbsp;
            <span aria-current='page' class='page-numbers current'>1</span>
            <a class='page-numbers' href=''>2</a>
            &nbsp;&hellip;&nbsp;
            <a class='page-numbers' href=''>5</a>
            <a class='page-numbers' href=''>6</a>&nbsp;
            <a class='page-numbers' href=''>&gt;</a>
        </div>
    </div>
</div>