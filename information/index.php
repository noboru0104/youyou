<!doctype html>
<html lang="ja">
<head>
<?php include("../head.php"); ?>
<title>運営者情報｜おかやま商工会エリア 遊youさんぽ</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/css/information/information.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/js/information/information.js"></script>
<script type="text/javascript" src="/js/common/jquery.matchHeight.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../header.php"); ?>
<div id="wrapper">
    <div class="l-block01">
    	<div class="l-inner700">
            <p class="p-image"><img src="/images/information/img_information01_pc.png" alt="岡山の見たい食べたい買いたいをご紹介！"></p>
            <p class="p-message">
            	岡山県の各商工会エリアには、隠れたおすすめスポットがたくさんあり、私たち商工会女性部はそんな情報をたくさん知っています。<br>
                この「あっ晴れ岡山 遊youさんぽ」では、そんな情報の中から厳選した「おすすめスポット」を一挙紹介いたします。ご夫婦で、お友達同士で、ご家族で、ぜひお楽しみください。
            </p>          
        </div>
        <div class="l-inner">
            <div class="l-block01-table">
                <p>
                    <a href=""><img class="is-imgChange" src="/images/top/btn_block01_01_pc.png" alt="グルメ"></a>
                </p>
                <p>
                    <a href=""><img class="is-imgChange" src="/images/top/btn_block01_02_pc.png" alt="ショッピング"></a>
                </p>
                <p>
                    <a href=""><img class="is-imgChange" src="/images/top/btn_block01_03_pc.png" alt="みどころ"></a>
                </p>
            </div>
        </div> 
    </div>
    
    <div class="l-block02">
    	<div class="l-inner1000">
            <h2><img class="is-imgChange" src="/images/information/h2_block02_pc.png" alt="ご挨拶"></h2>
            <div class="l-block02-table">
                <div class="l-left">
                	<p class="p-image"><img class="is-imgChange" src="/images/information/img_block02_pc.png" alt="岡山県商工会女性部連合会　会長　日神山　千代子"></p>
                    <p class="p-name"><span>岡山県商工会女性部連合会　会長</span>日神山　千代子</p>
                </div>
                <div class="l-right">
                	<p class="p-message">
                    	私たち商工会女性部は普段から地元にアンテナを張っているので、エリアの情報をたくさん持っています。それを県内外の多くの方にお役に立てていただきたいという思いから、この「あっ晴れ岡山 遊youさんぽ」を制作いたしました。岡山県の商工会エリアには隠れたおすすめスポットがたくさんあります。その中から、中高年女性がご夫婦やお友達と一緒に、またお孫さん連れで楽しめるスポットを中心に紹介しました。この一冊をご利用いただき、県内外の皆さんに、岡山を隅から隅まで楽しんでいただければ幸いです。
                    </p>
                    <p class="p-message">
                    	最後に「あっ晴れ岡山 遊youさんぽ」を制作するにあたって、各商工会女性部に多大なるご協力をいただきましたことを深く感謝いたします。
                    </p>
                    <p class="p-message">
                    	ありがとうございました。
                    </p>
                </div>             
            </div>  
        </div> 
    </div>
    
    <div class="l-block03">
    	<div class="l-inner1000">
            <h2><img class="is-imgChange" src="/images/information/h2_block03_pc.png" alt="岡山商工会"></h2>
            <div class="l-block03-table">
            	<div class="l-all">
                	<p class="p-title">岡山商工会連合会</p>
                    <p class="p-adress">
                    	岡山県商工会連合会 〒700-0817　岡山市北区弓之町4-19-401<br>
                        TEL：086-224-4341                     
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">岡山北商工会</p>
                    <p class="p-adress">
                    	〒709-2121　岡山市北区御津宇垣1630-1<br>
                        TEL：086-724-2131
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">岡山西商工会</p>
                    <p class="p-adress">
                    	〒701-0153　岡山市北区庭瀬488-6<br>
                        TEL：086-293-0454
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">岡山南商工会</p>
                    <p class="p-adress">
                    	〒701-0221　岡山市南区藤田564-131<br>
                        TEL：086-296-0765
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">吉備中央町商工会</p>
                    <p class="p-adress">
                    	〒716-1101　加賀郡吉備中央町豊野1-1<br>
                        TEL：0866-54-1062
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">瀬戸内市商工会</p>
                    <p class="p-adress">
                    	〒701-4246　瀬戸内市邑久町山田庄182-4<br>
                        TEL：0869-22-1010
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">赤磐商工会</p>
                    <p class="p-adress">
                    	〒709-0816　赤磐市下市357-7<br>
                        TEL：086-955-0144
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">備前東商工会</p>
                    <p class="p-adress">
                    	〒701-3202　備前市日生町寒河2570-31<br>
                        TEL：0869-72-2151
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">和気商工会</p>
                    <p class="p-adress">
                    	〒709-0422　和気郡和気町尺所2<br>
                        TEL：0869-93-0522
                    </p> 
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">つくぼ商工会</p>
                    <p class="p-adress">
                    	〒710-1101　倉敷市茶屋町2087<br>
                        TEL：086-428-0256
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">総社吉備路商工会</p>
                    <p class="p-adress">
                    	〒719-1162　総社市岡谷160<br>
                        TEL：0866-93-8000
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">真備船穂商工会</p>
                    <p class="p-adress">
                    	〒710-1301　倉敷市真備町箭田1141-1<br>
                        TEL：086-698-0265
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">浅口商工会</p>
                    <p class="p-adress">
                    	〒719-0243　浅口市鴨方町鴨方2244-8<br>
                        TEL：0865-44-3211
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">備中西商工会</p>
                    <p class="p-adress">
                    	〒714-1202　小田郡矢掛町小林163-2<br>
                        TEL：0866-82-0559
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">備北商工会</p>
                    <p class="p-adress">
                    	〒716-0111　高梁市成羽町下原432-1<br>
                        TEL：0866-42-2412
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">阿哲商工会</p>
                    <p class="p-adress">
                    	〒719-3611　新見市神郷下神代4898-9<br>
                        TEL：0867-92-6103
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">真庭商工会</p>
                    <p class="p-adress">
                    	〒719-3214　真庭市鍋屋6<br>
                        TEL：0867-42-4325
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">作州津山商工会</p>
                    <p class="p-adress">
                    	〒708-1205　津山市新野東567-9<br>
                        TEL：0868-36-5533
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">鏡野町商工会</p>
                    <p class="p-adress">
                    	〒708-0324　苫田郡鏡野町竹田747<br>
                        TEL：0868-54-3311
                    </p>
                </div>            
            </div>
            <div class="l-block03-table">
            	<div class="l-left">
                	<p class="p-title">久米郡商工会</p>
                    <p class="p-adress">
                    	〒709-3717　久米郡美咲町原田1757-8<br>
                        TEL：0868-66-0033
                    </p>
                </div>
                <div class="l-right">
                	<p class="p-title">みまさか商工会</p>
                    <p class="p-adress">
                    	〒707-0025　美作市栄町187-4<br>
                        TEL：0868-73-6520
                    </p>
                </div>            
            </div>
        </div> 
    </div>
    
    <?php include("../information.php"); ?> 
    
</div>
<?php include("../footer.php"); ?>
</body>
</html>
