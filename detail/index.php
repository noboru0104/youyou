<!doctype html>
<html lang="ja">
<head>
<?php include("../head.php"); ?>
<title>そば処 ひじり庵｜おかやま商工会エリア 遊youさんぽ</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/css/detail/detail.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/js/detail/detail.js"></script>
<script type="text/javascript" src="/js/common/jquery.matchHeight.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("../header.php"); ?>
<div id="wrapper">
    <div class="l-block01">
    	<div class="l-inner700">
            <h2 class="p-title p-gourmet">そば処 ひじり庵</h2>
            <div class="l-block01-table">
            	<p class="p-date">2019/02/25(Mon)</p>
                <p class="p-category"><span class="p-gourmet">グルメ</span><span class="p-area01">岡山北</span></p>
            </div>
            <p class="p-image"><img src="/images/detail/img_detail_01_pc.png" alt="そば処 ひじり庵"></p>
            <p class="p-message">
            	東京のそば店でも働いていた店主が、呉服店だった日本家屋を改装し、念願の店をオープン。オリジナルブレンドの粉を使った自家製そばのほか、「そばデミカツ丼セット」や「そば天ぷらセット」などの定食も提供しています。食後は庭にある足湯に浸かり、のんびりと過ごすのはいかがですか。
            </p>            
        </div>
    </div>
    <div class="l-block02">
    	<div class="l-inner1000">
            <div class="l-block02-01">
            	<div class="l-left">
                	<table>
                    	<tr>
                        	<th>住　　所</th>
                            <td>岡山市北区菅野2879-1</td>
                        </tr>
                    	<tr>
                        	<th>電話番号</th>
                            <td>070-5304-2266</td>
                        </tr>
                    	<tr>
                        	<th>営業時間</th>
                            <td>11:00～14:30（売切れ次第終了）</td>
                        </tr>
                    	<tr>
                        	<th>休　　日</th>
                            <td>木曜</td>
                        </tr>
                    	<tr>
                        	<th>席</th>
                            <td>28席</td>
                        </tr>
                    	<tr>
                        	<th>駐 車 場</th>
                            <td>15台</td>
                        </tr>
                    </table>
                </div><div class="l-right">
                	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3278.3614900274533!2d133.88630831523346!3d34.746485680422914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3554045fa3fc156d%3A0x97eff117b4138b21!2z44CSNzAxLTExNDEg5bKh5bGx55yM5bKh5bGx5biC5YyX5Yy66I-F6YeO77yS77yY77yX77yZ4oiS77yR!5e0!3m2!1sja!2sjp!4v1551407888173" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>       
        </div>
    </div>
    
    <?php include("../information.php"); ?> 
    
</div>
<?php include("../footer.php"); ?>
</body>
</html>
