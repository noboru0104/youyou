<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'ai111i6nad_test' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'ai111i6nad' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'tWtzbCKz' );

/** MySQL のホスト名 */
define( 'DB_HOST', '127.0.0.1:3306' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j0ycBI3Y<n0`Xn#Oc8H<RD;fpSpe^1~[h^12*gG&^CPEsU5$_e=^vAbw)@+YFIMp' );
define( 'SECURE_AUTH_KEY',  'Oh*Ocur9cKM];T1M$T6lByrJ<OW,wbp^-}q+JAnA*j`To:2R15kHjI([4|jgEQ:G' );
define( 'LOGGED_IN_KEY',    '>ad]b4g6-JUE``w/c5*T~+w5,{]?sG3fQ 0Q#m@Gs~)uL#yw6CPfsROhqm7A.w?g' );
define( 'NONCE_KEY',        'R/oNkaP`Vi>*-b B-AQQ~<s}d`IEquM<rwS/S~RT!U8c0s@:CwHMxtyqfYhM^kbp' );
define( 'AUTH_SALT',        '<f-3Cy;!%u%76|9P!4,;=[;l6aQ!5J#0aJxPRIwHJg*smlv]JaxL(1f015D{{/By' );
define( 'SECURE_AUTH_SALT', 'J|9V>TK3 LyK,4Z]xTRvD6ioq6jh?sWQ8j5r,rudYR`F1BKSKR=%9M&mXbQysw}O' );
define( 'LOGGED_IN_SALT',   '0~Prn.V$%/FM$f?w@vj3@)t2CIQXx`>e=/OQ#quCD`%lvl2i^(;~~/;^w&_aOOWj' );
define( 'NONCE_SALT',       ',:>>#=$D]:RyG(_t!{0Oa>1x!|JddL;7/J*Ip6T9,h9{sMFFs(X73CUNzx_eB)<m' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
