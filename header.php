<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left">
                <a href="/"><img class="is-imgChange" src="/images/common/btn_header_logo_pc.png" alt="おかやま商工会エリア 遊youさんぽ"></a>
            </div>
            <div class="l-header-center">
            	<h1>岡山の「見たい」「食べたい」「買いたい」をご紹介！ 遊youさんぽ</h1>
                <div>
                    <div><a class="p-gourmet" href="">グルメ<span>GOURMET</span></a></div>
                    <div><a class="p-shopping" href="">ショッピング<span>SHOPPING</span></a></div>
                    <div><a class="p-spot" href="">みどころ<span>SPOT</span></a></div>
                    <div><a class="p-search" href="">エリア別に探す<span>SEARCH</span></a></div>
                    <div><a class="p-information" href="">運営者情報<span>INFORMATION</span></a></div>
                </div>
            </div>
            <div class="l-header-right">
            	<!--SPハンバーガーメニュー-->
                <div id="toggle_block" class="sponly0">
                	
                    <div id="toggle">
                        <div id="toggle-btn">
                            <span id="toggle-btn-icon"></span>
                        </div>
                    </div><!--/#toggle-->
                </div><!--/#toggle_block-->
            </div>
        </div>
              
        <nav>
            <div id="global-nav-area">
                <ul id="global-nav">
                    <li>
                    	<p><a href=""><img src="/images/common/btn_header_01_sp.png" alt="グルメ GOURMET"></a></p>
                    </li><li>
                    	<p><a href=""><img src="/images/common/btn_header_02_sp.png" alt="ショッピング SHOPPING"></a></p>
                    </li><li>
                    	<p><a href=""><img src="/images/common/btn_header_03_sp.png" alt="みどころ SPOT"></a></p>
                    </li><li>
                    	<p><a href=""><img src="/images/common/btn_header_04_sp.png" alt="エリア別に探す SEARCH"></a></p>
                    </li><li>
                    	<p><a href=""><img src="/images/common/btn_header_05_sp.png" alt="運営者情報 INFORMATION"></a></p>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>