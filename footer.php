<footer>
    <div class="l-footerBlock01">
    	<div class="l-inner1100">
        	<div class="l-footerBlock01-table">
            	<div class="l-left">
                	<p><img class="is-imgChange" src="/images/common/btn_footer_logo_pc.png" alt="おかやま商工会エリア 遊youさんぽ"></p>
                </div>
                <div class="l-right">
                	<p class="l-link"><a href="">トップ</a><a href="">グルメ</a><a href="">ショッピング</a><a href="">みどころ</a><a href="">エリア別に探す</a><a href="">運営者情報</a></p>
                    <p class="l-adress">
                        岡山商工会女性部連合会<br>
                        〒000-0000　岡山県岡山市○○○○○○○-○○
                    </p>
                    <div class="l-banner">
                    	<p>
                        	<a href=""><img class="is-imgChange p-banner01" src="/images/common/btn_footer_01_pc.png" alt="岡山商工会連合会"></a>
                        </p><p>
                        	<a href=""><img class="is-imgChange p-banner02" src="/images/common/btn_footer_02_pc.png" alt="商工会女性部発 岡山おひさま便"></a>
                        </p><p>
                        	<a href=""><img class="is-imgChange p-banner03" src="/images/common/btn_footer_03_pc.png" alt="岡山県定住促進 U・I・Jターンで地域を元気に！"></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-copy">
    	<p>&copy;youyousanpo All Rights Reserved. </p>
    </div>
</footer>