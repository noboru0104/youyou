<!doctype html>
<html lang="ja">
<head>
<?php include("head.php"); ?>
<title>おかやま商工会エリア 遊youさんぽ</title>
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="/css/common/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="/css/top.css">
<link rel="stylesheet" type="text/css" href="/css/common/animate.css">
<!-- ▲個別CSS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="/js/top.js"></script>
<script type="text/javascript" src="/js/common/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/js/common/jquery.bxslider.min.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<?php include("header.php"); ?>
<div class="l-mvBlock">
    <div class="l-mv">
        <ul class="bxslider">
            <li><img class="is-imgChange" src="/images/top/img_mv01_pc.png" alt=""></li>
            <li><img class="is-imgChange" src="/images/top/img_mv02_pc.png" alt=""></li>
            <li><img class="is-imgChange" src="/images/top/img_mv03_pc.png" alt=""></li>
        </ul>
           
    </div>
</div>
    
<div id="wrapper">
    <div class="l-block01">
    	<div class="l-inner">
            <h2><img class="is-imgChange" src="/images/top/h2_block01_pc.png" alt="メニュー"></h2>
            <div class="l-block01-table">
                <p>
                    <a href=""><img class="is-imgChange" src="/images/top/btn_block01_01_pc.png" alt="グルメ"></a>
                </p>
                <p>
                    <a href=""><img class="is-imgChange" src="/images/top/btn_block01_02_pc.png" alt="ショッピング"></a>
                </p>
                <p>
                    <a href=""><img class="is-imgChange" src="/images/top/btn_block01_03_pc.png" alt="みどころ"></a>
                </p>
            </div>
        </div>
    </div>
    
    <?php include("information.php"); ?>
    <?php include("search01.php"); ?>    
    
</div>
<?php include("footer.php"); ?>
</body>
</html>
